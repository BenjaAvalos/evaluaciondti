<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $faker= \Faker\Factory::create();

        for($i=0;$i<15;$i++){
        DB::table('users')->insert([

            'nombre' => $faker->name(),
            'email' => $faker->email(),
            'email_verified_at' => $faker->date('Y-d-m H:i:s'),
            'password' => Hash::make('password'),
            'created_at' => $faker->date('Y-d-m H:i:s'),
            'updated_at' => $faker->date('Y-d-m H:i:s'),
            ]);
        }
    }
}