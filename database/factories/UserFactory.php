<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = \App\Models\User::class;
    
    

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->name(),
            'email' => $this->faker->unique()->safeEmail(),
            'password' => Str::random(10),
            'remember_token' => Str::random(10),
            'created_at' => $this->faker->date($format('Y-d-m H:i:s')),
            'updated_at' => $this->faker->date('Y-d-m H:i:s'),
        ];
    }
}
