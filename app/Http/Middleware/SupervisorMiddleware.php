<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class SupervisorMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        if(! $request -> user()->id == DB::select('select user_id from role_user INNER JOIN roles on role_user.role_id = roles.id where role_id=3')){
            
            return redirect('home');

        }

        return $next($request);
    }
}
