<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Middleware\Authenticate as Middleware;

class AdminMiddleware extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    protected function redirectTo($request)
    {
        if(! $request -> user()->id == DB::select('select user_id from role_user INNER JOIN roles on role_user.role_id = roles.id where role_id=1')){
            
            return redirect('home');

        }
        return $next($request);
    }
}
