@extends('layouts.principal')

@section('contenido')

<table class="table">
    <thead class="thead-dark">
    <tr>
    <th scope="col">ID</th>
    <th scope="col">Nombre</th>
    <th scope="col">E-mail</th>
    <th scope="col">Contraseña</th>
    <th scope="col">Token</th>
    <th scope="col">Creado</th>
    <th scope="col">Editado</th>
    <th scope="col" colspan="3" >Operaciones</th>
    </tr>
    </thead>

<tbody>
    @foreach ($users as $user)
    <tr>
    <td>{{$user->id}}</td>
    <td>{{$user->nombre}}</td>
    <td>{{$user->email}}</td>
    <td>{{$user->password}}</td>
    <td>{{$user->remember_token}}</td>
    <td>{{$user->created_at->format('Y-m-d')}}</td>
    <td>{{$user->updated_at->format('Y-m-d')}}</td>
    <td><a href="{{route('recursos.edit',$user->id)}}" class="btn btn-primary">Editar</a></td>
    <td><form method="POST" action="{{route('recursos.destroy', $user->id)}}">
        @method('DELETE')
        @csrf
        <button class="btn btn-danger" type="submit">Eliminar</button>
        </form></td>
    </tr>
    @endforeach
    </tbody>
    </table>
@endsection