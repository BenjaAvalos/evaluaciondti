@extends('layouts.principal')
                    
@section('contenido')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Registrarse') }}</div>
                    <div class="card-body">

                        @include('layouts.errores')
                        
                        <form action="{{route("recursos.store")}}" method="POST">
                            @include('layouts.form_create_user')
                            <br><input type="submit" value="Enviar" class="btn btn-primary">
                        </form>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection