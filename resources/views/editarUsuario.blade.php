@extends('layouts.principal')
                    
@section('contenido')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Editar') }}</div>
                    <div class="card-body">

                        @include('layouts.errores')
                        
                        <form action="{{route("recursos.update", $user->id)}}" method="POST">
                            @method('PUT')
                            @include('layouts.form_edit_user')
                            <br><input type="submit" value="Enviar" class="btn btn-primary">
                        </form>
                    </div>
            </div>
        </div>
    </div>
</div>
@endsection