@extends('layouts.principal')

@section('contenido')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8" style="text-align: center;">
            <center><br><br><br><br><br><br><br>
                <button onclick="location.href='{{ route('recursos.create') }}'" class="btn btn-primary" style="height:200px; width:200px">Registrar</button>
            
                <button onclick="location.href='{{ route('recursos.index') }}'" class="btn btn-primary" style="height:200px; width:200px">Listar</button>
        </center>
        </div>
    </div>
</div>
@endsection
