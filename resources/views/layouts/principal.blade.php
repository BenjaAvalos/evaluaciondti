@extends('layouts.app')

@section('content')
<div class="container">

        @if (session('status'))
        <div class="alert alert-success">
        {{session('status')}}
        </div>
        @endif

        @yield('contenido')

        <blockquote class="blockquote text-center">
        <br><br><p class="mb-0">Fiscalia General del Estado</p>
        <footer class="footer"><cite title="Source Title">FGE</cite></footer>
        </blockquote>
</div>
@endsection